#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.options
import requests
import time
import json
import threading
from tornado.options import define, options
from threading import Thread




class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(".*", MainHandler)]
        settings = dict(debug=False)
        tornado.web.Application.__init__(self, handlers, **settings)



class MainHandler(tornado.websocket.WebSocketHandler):

    def check_origin(self, origin):
        self.path = self.request.uri
        commandArray = self.path[1:].split("/")
        self.symbolList = []
        for command in commandArray: 
            symbol =  command.split("@")[0]
            self.symbolList.append(symbol)
            
        return True
    
    def requestD(self,target,symbol):
        while ws_clients:
            time.sleep(0.5)
            response = requests.request("GET", "https://satangcorp.com/api/v3/depth?symbol="+ symbol, headers={}, data={})
            jsonData = json.loads(response.text)
            if("bids" in jsonData):
                dataBack[symbol] = json.dumps({
                    "s": symbol,
                    "b": jsonData["bids"],
                    "a": jsonData["asks"]
                })
                target.write_message(dataBack[symbol])

    def open(self):
        if self not in ws_clients:
            ws_clients.append(self)
            breakEvent = True
            time.sleep(1)

        breakEvent = False
        for symbol in self.symbolList: 
            if(symbol not in treadAll):
                treadAll[symbol] = Thread(target=self.requestD,args=[self,symbol])
                treadAll[symbol].start()

        # while ws_clients and not breakEvent:
        #     try:
        #         time.sleep(1)
        #         print(ws_clients)
        #         for symbol in self.symbolList: 
        #             if(symbol in dataBack):
        #                 try:
        #                     for c in ws_clients:
        #                         c.write_message(dataBack[symbol])
        #                 except Exception:
        #                     return 
        #     except Exception:
        #         return 

        
  
    def on_connection_close(self):
        breakEvent = True


                    
            
    def on_close(self):
        logging.info("A client disconnected")
        breakEvent = True
        if self in ws_clients:
            ws_clients.remove(self)


    def on_message(self, message):
        pass

    write_message(dataBack[symbol])




ws_clients = []
breakEvent = False
dataBack = {}
treadAll = {}

app = Application()
define("port", default=8000, help="run on the given port", type=int)
looper = tornado.ioloop.IOLoop.instance()
try: 
    app.listen(options.port)
    looper.start()
except KeyboardInterrupt:
    looper.stop()
        



